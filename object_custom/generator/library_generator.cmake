#
# Make a static library containing generated code
#
function(add_generated_library)
  set(_CODE_GEN_NAME ${ARGV0})
  set(options STATIC SHARED)
  set(oneValueArgs WORKING_DIRECTORY COMMENT)
  set(multiValueArgs OUTPUT INCLUDES DEPENDS COMMAND)

  cmake_parse_arguments(PARSE_ARGV 1 _CODE_GEN
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}")

  set(_libtype STATIC)
  if (${_CODE_GEN_SHARED})
    set(_libtype SHARED)
  endif()

  message(STATUS "Code generator: ${_CODE_GEN_NAME} ${_libtype}")
  message(STATUS " OUTPUT            ${_CODE_GEN_OUTPUT}")
  message(STATUS " INCLUDES          ${_CODE_GEN_INCLUDES}")
  message(STATUS " DEPENDS           ${_CODE_GEN_DEPENDS}")
  message(STATUS " WORKING_DIRECTORY ${_CODE_GEN_WORKING_DIRECTORY}")
  message(STATUS " COMMAND           ${_CODE_GEN_COMMAND}")

  file(MAKE_DIRECTORY ${_CODE_GEN_WORKING_DIRECTORY})

  add_custom_command(
    COMMENT "${_CODE_GEN_COMMENT}"
    OUTPUT ${_CODE_GEN_OUTPUT}
    DEPENDS ${_CODE_GEN_DEPENDS}
    WORKING_DIRECTORY ${_CODE_GEN_WORKING_DIRECTORY}
    COMMAND ${_CODE_GEN_COMMAND})

  string(MD5 rand_suffix "${_CODE_GEN_OUTPUT} dep ${_CODE_GEN_DEPENDS} cmd ${_CODE_GEN_COMMAND}")

  add_custom_target(
    code_generator_${rand_suffix}
    DEPENDS ${_CODE_GEN_OUTPUT})

  add_library(${_CODE_GEN_NAME} ${_libtype} ${_CODE_GEN_OUTPUT})
  add_dependencies(${_CODE_GEN_NAME} code_generator_${rand_suffix})

  message(STATUS " Code generator target is 'code_generator_${rand_suffix}'")

  target_include_directories(${_CODE_GEN_NAME}
    INTERFACE
      ${_CODE_GEN_INCLUDES})

endfunction()

