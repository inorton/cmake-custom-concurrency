#!/usr/bin/python
"""
A very simple source generator
"""

import os
import sys
import random
import time


def generate_header(prefix, filename):
    """
    Generate a C header file

    :param prefix: prefix functions with this
    :param filename: header file name to write

    """
    if os.path.exists(filename):
        print "Generator run concurrently for {}".format(filename)

    with open(filename, "w") as outfile:
        outfile.write("""
        int {}_function(void);
        """.format(prefix))

def generate_source(prefix, headerfile, filename):
    """
    Generate a C Source file

    :param prefix: prefix functions with this
    :param headefile: the header to include
    :param filename: header file name to write

    """
    if os.path.exists(filename):
        print "Generator run concurrently for {}".format(filename)

    with open(filename, "w") as outfile:
        outfile.write("#include \"{}\"\n".format(os.path.basename(headerfile)))
        outfile.write("int {}_function(void)\n".format(prefix))
        outfile.write("{\n")
        outfile.write("  return {};\n".format(random.randint(1,33333)))
        outfile.write("}\n")

def generate(prefix, header, source):
    if os.path.exists(header):
        os.unlink(header)
    if os.path.exists(source):
        os.unlink(source)
    generate_header(prefix, header)
    generate_source(prefix, header, source)

print "Starting source generator PID={}".format(os.getpid())

busyfile = "busy.txt"
assert not os.path.exists(busyfile), "multiple generators invoked!"
open(busyfile, "w").close()
time.sleep(2)
try:
    for i in range(4):
        prefix = "test{}".format(i)
        header = "test{}.h".format(i)
        source = "test{}.c".format(i)
        print "{} generate {} {} {}".format(os.getpid(), prefix, header, source)
        generate(prefix, header, source)
finally:
    os.unlink(busyfile)
