#!/usr/bin/python

import os
import sys
import hashlib
import time
import subprocess

args = sys.argv[1:]

def digest(args, envs):
    md5 = hashlib.md5()
    for arg in args:
        md5.update(arg)
    for env in envs:
        md5.update(env)
        md5.update(envs[env])
    return md5.hexdigest()


envs = {}
for name in os.environ:
    if "PATH" in name:
        envs[name] = os.getenv(name)

lockdir = os.path.join(os.getcwd(), ".lck", digest(args, envs))

try:
    while True:
        try:
            os.makedirs(lockdir)
            break
        except:
            time.sleep(0.3)
    try:
        subprocess.check_call(args, shell=False, cwd=os.getcwd())
    except Exception as err:
        print err
finally:
    os.rmdir(lockdir)







